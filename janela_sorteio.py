from tkinter.filedialog import askopenfilename
from tkinter import *
from tkinter import ttk
from random import randint
from arquivos_globais import *
from janela_atividade import CadastroAtividades
from utilitarios import *


class Sorteio:
    def __init__(self, master=None):

        #---------------Organizar Janela Principal em Grid--------------------

        self.container1 = Frame(master, width=400, height=300)
        self.container1["background"] = background_color
        self.container1.grid(row=0, column=0)

        self.container2 = Frame(master, width=400, height=300)
        self.container2["background"] = background_color
        self.container2.grid(row=0, column=1)

        self.container3 = Frame(master, width=400, height=60)
        self.container3["background"] = background_color
        self.container3.grid(row=1, column=0)

        self.container4 = Frame(master, width=400, height=60)
        self.container4["background"] = background_color
        self.container4.grid(row=1, column=1)

        self.container_sortear = Frame(master, width=400, height=60)
        self.container_sortear["background"] = background_color
        self.container_sortear.grid(row=2, column=0, columnspan=2)

        self.container5 = Frame(master, width=800, height=60)
        self.container5["background"] = background_color
        self.container5.grid(row=3, column=0, columnspan=2)

        #---------------Configurar Titulos Principais da Janela-----------------

        self.title_atividades = Label(self.container1, text="Atividades")
        self.title_atividades["font"] = (font, "20", "bold")
        self.title_atividades["background"] = background_color
        self.title_atividades["foreground"] = foreground_color
        self.title_atividades.place(x=120, y=10)

        self.title_alunos = Label(self.container2, text="Alunos")
        self.title_alunos["font"] = (font, "20", "bold")
        self.title_alunos["background"] = background_color
        self.title_alunos["foreground"] = foreground_color
        self.title_alunos.place(x=150, y=10)

        #--------------------Configurar botões da Janela------------------------

        self.btn_alunos = Button(self.container4, width = 20, text="importar alunos", relief="flat")
        self.btn_alunos["command"] = self.importarAlunos
        self.btn_alunos["cursor"] = "hand2"
        self.btn_alunos["font"] = (font, "10", "bold")
        self.btn_alunos["background"] = foreground_color
        self.btn_alunos["foreground"] = background_color
        self.btn_alunos.place(relx=0.5, rely=0.5, anchor=CENTER)

        self.btn_atividade = \
        Button(self.container3, width = 20, relief="flat", command=self.cadastrarAtividade)
        self.btn_atividade["text"] = "cadastrar atividade"
        self.btn_atividade["cursor"] = "hand2"
        self.btn_atividade["font"] = (font, "10", "bold")
        self.btn_atividade["background"] = foreground_color
        self.btn_atividade["foreground"] = background_color
        self.btn_atividade.place(relx=0.5, rely=0.5, anchor=CENTER)

        self.btn_sortear = \
        Button(self.container_sortear, width = 10, text="sortear", relief="flat",command=self.sortearAlunos)
        self.btn_sortear["cursor"] = "hand2"
        self.btn_sortear["font"] = (font, "10", "bold")
        self.btn_sortear["background"] = foreground_color
        self.btn_sortear["foreground"] = background_color
        self.btn_sortear.place(relx=0.5, rely=0.5, anchor=CENTER)

        #---------------Configurar Label de RESULTADO---------------------------

        self.resultado = Label(self.container5, text = "RESULTADO", relief="solid", width=60, height=2)
        self.resultado["font"] = (font, "12", "bold")
        self.resultado["background"] = background_color
        self.resultado["foreground"] = foreground_color
        self.resultado.place(relx=0.5, rely=0.5, anchor=CENTER)

        #------------------Configurar estilo da TreeView------------------------

        self.style = ttk.Style()
        self.style.configure("mystyle.Treeview", highlightthickness=0, bd=0, font=(font, 10))
        self.style.configure("mystyle.Treeview.Heading", font=('Segoe UI', 10,'bold'), foreground=foreground_color)
        self.style.layout("mystyle.Treeview", [('mystyle.Treeview.treearea', {'sticky': 'nswe'})])

        #------------Configurar exibição da TreeView(tabela) alunos--------------

        self.tabela_alunos = \
        ttk.Treeview(self.container2, selectmode='browse', height=7, style="mystyle.Treeview")
        self.tabela_alunos.place(x=30, y=95)

        self.rolagem_alunos = \
        ttk.Scrollbar(self.container2, orient="vertical", command=self.tabela_alunos.yview)
        self.rolagem_alunos.place(x=30+200+2+150-50, y=95, height=165)

        self.tabela_alunos.configure(yscrollcommand=self.rolagem_alunos.set)
        self.tabela_alunos["columns"] = ("1", "2")
        self.tabela_alunos['show'] = 'headings'
        self.tabela_alunos.column("1", width=50, anchor='c')
        self.tabela_alunos.column("2", width=250, anchor='w')
        self.tabela_alunos.heading("1", text="Nº")
        self.tabela_alunos.heading("2", text="Nome")

        #--------------------Preencher TreeView(tabela) alunos---------------------

        for i in range(len(alunos)):
            self.tabela_alunos.insert("", i, text="",values=(i+1,"  "+alunos[i]))

        #-----------Configurar exibição da TreeView(tabela) atividades-------------

        self.tabela_atividades = \
        ttk.Treeview(self.container1, selectmode='browse', height=7, style="mystyle.Treeview")
        self.tabela_atividades.place(x=30, y=95)

        self.rolagem_atividades = \
        ttk.Scrollbar(self.container1, orient="vertical", command=self.tabela_atividades.yview)
        self.rolagem_atividades.place(x=30+200+2+150-50, y=95, height=165)

        self.tabela_atividades.configure(yscrollcommand=self.rolagem_atividades.set)
        self.tabela_atividades["columns"] = ("1", "2")
        self.tabela_atividades['show'] = 'headings'
        self.tabela_atividades.column("1", width=50, anchor='c')
        self.tabela_atividades.column("2", width=250, anchor='w')
        self.tabela_atividades.heading("1", text="Cod")
        self.tabela_atividades.heading("2", text="Atividades")

        #------------------Preencher TreeView(tabela) atividades-------------------

        for i in range(len(atividades)):
            self.tabela_atividades.insert("", i, text="",values=(i+1,"  "+atividades[i]['nome']))


    def sortearAlunos(self):
        if len(atividades) != 0 and len(alunos) != 0:
            self.resultado["text"] = atividades[-1]['nome'] \
            + " - " + alunos[randint(0, len(alunos) - 1)]

        elif len(atividades) == 0 and len(alunos) != 0:
            self.resultado["text"] = "INSIRA UMA ATIVIDADE"

        elif len(atividades) != 0 and len(alunos) == 0:
            self.resultado["text"] = "INSIRA OS ALUNOS"

        else:
            self.resultado["text"] = "INSIRA AS ATIVIDADES E OS ALUNOS"


    def importarAlunos(self):
        self.diretorio = askopenfilename(filetypes=(("arquivos CSV", "*.csv"),))
        #--------Limpar tabela alunos
        self.tabela_alunos.delete(*self.tabela_alunos.get_children())
        #s
        importar_alunos(self.diretorio)
        #
        #--------Atualizar tabela alunos
        for i in range(len(alunos)):
            self.tabela_alunos.insert("", i, text="",values=(i+1,"  "+alunos[i]))


    def cadastrarAtividade(self):

        tela_atividade = Tk()
        tela_atividade["background"] = background_color
        tela_atividade.geometry(size_window)
        tela_atividade.title(title_window)
        tela_atividade.resizable(0, 0)

        self.len_original = len(atividades)-1

        CadastroAtividades(tela_atividade)
        self.ultima_posicao = len(atividades)-1
        
        tela_atividade.mainloop()

        self.ultima_posicao = len(atividades)-1

        if self.ultima_posicao > self.len_original:
            self.tabela_atividades.insert\
            ("", self.ultima_posicao, text="",values=(len(atividades),"  "+atividades[self.ultima_posicao]['nome']))


root = Tk()
root["background"] = background_color
root.geometry(size_window)
root.title(title_window)
root.resizable(0, 0)

Sorteio(root)
root.mainloop()