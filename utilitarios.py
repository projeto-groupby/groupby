from random import randint
from arquivos_globais import *


def importar_alunos(nome_arquivo):
    with open(nome_arquivo, encoding = "iso-8859-1") as arquivo:
        nomes = arquivo.read()
        nomes = [nome_aluno for nome_aluno in nomes.split('\n') if nome_aluno != '']

    del alunos[:]
    for nome in nomes:
        nome = nome.replace("Ö", "Í")
        nome = nome.replace("à", "Ó")
        nome = nome.replace("µ", "ÍL")
        nome.encode('utf-8')

        alunos.append(nome)


def sortear_aluno(alunos):  
    return alunos[randint(0, len(alunos) - 1)]


class Atividade:

    def __init__(self, nome, descricao, data):
        self.nome = nome
        self.descricao = descricao
        self.data = data

