from tkinter import *
from arquivos_globais import *
from utilitarios import *

class CadastroAtividades:
    def __init__(self, master=None):
        global main_window
        main_window = master

        self.container1 = Frame(master)
        self.container1["background"] = background_color
        self.container1.pack()

        self.container2 = Frame(master)
        self.container2["background"] = background_color
        self.container2.pack()

        self.container3 = Frame(master)
        self.container3["background"] = background_color
        self.container3.pack()

        self.container4 = Frame(master)
        self.container4["background"] = background_color
        self.container4.pack()

        self.container5 = Frame(master)
        self.container5["background"] = background_color
        self.container5.pack()

        #Criar titulo
        self.title = Label(self.container1, text="Cadastrar atividade")
        self.title["font"] = (font, "20", "bold")
        self.title["background"] = background_color
        self.title["foreground"] = foreground_color
        self.title.pack(pady=10)

        #Criar Label "Atividade"
        self.atividade_label = Label(self.container2)
        self.criarLabel(self.atividade_label, self.container2, "Atividade", 10)
        #Criar Input atividade
        self.atividade = Entry(self.container2, width = 30, relief="solid", font=(font, "10", "normal"))
        self.atividade.pack(pady=0, ipady=3)

        #Criar Label "Data"
        self.date_label = Label(self.container3)
        self.criarLabel(self.date_label, self.container3, "Data", 10)
        #Criar Input data
        self.date = Entry(self.container3, width = 30, relief="solid", font=(font, "10", "normal"))
        self.date.pack(pady=0, ipady=3)

        #Criar Label "Descrição"
        self.text_label = Label(self.container4)
        self.criarLabel(self.text_label, self.container4, "Descrição", 10)
        #Criar Input descrição
        self.text = Text(self.container4, height=6, width=30, relief="solid")
        self.text["font"] = (font, "10", "normal")
        self.text.pack(pady=10)

        self.button = \
        Button(self.container5, width = 10, text="cadastrar", relief="flat", command=self.cadastrarAtividade)
        self.button["cursor"] = "hand2"
        self.button["font"] = (font, "10", "bold")
        self.button["background"] = foreground_color
        self.button["foreground"] = background_color
        self.button.pack(pady=18)

        #Criar Label "Error"
        self.error_label = Label(self.container5, text = "")
        self.error_label["font"] = (font, "10", "bold")
        self.error_label["background"] = background_color
        self.error_label["foreground"] = "#eb4d4b"
        self.error_label.pack(pady=0)


    def cadastrarAtividade(self):
        #----------------------Recuperar informações do formulário-----------------------
        if self.atividade.get() != "" and self.date.get() != "" and self.text.get("1.0",END) != "":
            a = Atividade(self.atividade.get(), self.date.get(), self.text.get("1.0",END))
            atividades.append(a.__dict__)
            main_window.quit()
            main_window.destroy()
        elif self.atividade.get() == "":
            self.error_label["text"] = "Insira o nome da atividade"

        elif self.date.get() == "":
            self.error_label["text"] = "Insira a data da atividade"


    def criarLabel(self, label, container, text, pady):
        label = Label(container, text = text)
        label["font"] = (font, "12", "normal")
        label["background"] = background_color
        label["foreground"] = foreground_color
        label.pack(pady=pady)


if __name__ == "__main__":

    root = Tk()
    root["background"] = background_color
    root.geometry(size_window)
    root.title(title_window)
    root.resizable(0, 0)

    CadastroAtividades(root)
    root.mainloop()
