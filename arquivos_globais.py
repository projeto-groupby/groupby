#--------------------Definição de listas globais para manipular---------------------
global alunos
global atividades
#----------------------Definição de estilos globais da janela-----------------------
global background_color
global foreground_color
global font
global size_window
global title_window

#----------------------Dados de alunos e atividades--------------------------------

alunos = []
atividades = []

#--------------------------------Estilos da Janela----------------------------------
background_color = "#ecf0f1"
foreground_color = "#20bf6b"
font = "Segoe UI"
size_window = "800x480+283+100"
title_window = "GroupBy"